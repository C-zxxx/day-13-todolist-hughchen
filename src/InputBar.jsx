import { useState } from "react";
import "./InputBar.css";
import { initTodos } from "./todoSlice";
import * as api from "./apis/todo";
import { useTodo } from "./hooks/useTodo";
import { Button, Input, message } from "antd";

const InputBar = () => {
  const [newItem, setNewItem] = useState("");
  const PRESS_ENTER = 13;
  const { reloadTodos } = useTodo();
  const [buttonLoading, setButtonLoading] = useState(false);

  function onClick() {
    setButtonLoading(true);
    if (newItem.trim() === "") {
      setNewItem("");
      message.warning("Please input something~");
      setButtonLoading(false);
      return;
    }

    api
      .addTodo({
        id: new Date().getTime(),
        text: newItem,
        done: false,
      })
      .then((res) => {
        reloadTodos();
        message.success("Add successfully~");
        setButtonLoading(false);
        setNewItem("");
      })
      .catch((err) => {
        setButtonLoading(false);
      });
  }

  function pressEnter(e) {
    if(buttonLoading){
      return
    }
    if (e.keyCode === PRESS_ENTER) {
      onClick(e);
    }
  }

  function changeInputValue(e) {
    setNewItem(e.target.value);
  }

  return (
    <div className="inputBar">
      <Input
        className="addInput"
        type="text"
        value={newItem}
        placeholder="Please input your Todo Item"
        size="large"
        onChange={changeInputValue}
        onKeyUp={pressEnter}
      ></Input>
      <Button type="primary" size="small" onClick={onClick} loading={buttonLoading} disabled={buttonLoading}>
        ADD
      </Button>
    </div>
  );
};
export default InputBar;
