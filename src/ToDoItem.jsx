import { useDispatch } from "react-redux";
import { onChangeStatus, onDeleteIcon, onSaveEdit } from "./todoSlice";
import "./ToDoItem.css";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import * as api from "./apis/todo";
import { Button, message, Modal, Input, Popconfirm } from "antd";

const ToDoItem = (props) => {
  const dispatch = useDispatch();
  const [editText, setEditText] = useState("");
  const navigate = useNavigate();
  const inDoneList = useLocation().pathname === "/done";
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [saveLoding, setSaveLoding] = useState(false);
  const { TextArea } = Input;
  const PRESS_ENTER = 13;
  const Popconfirm_TITLE = "Are you sure to delete this item?"

  function changeStatus() {
    if (!inDoneList) {
      api
        .toggleTodo(props.item.id, !props.item.done)
        .then((res) => {
          message.success("Toggle successfully~");
          dispatch(onChangeStatus(props.item.id));
        })
        .catch((err) => {});
    } else {
      navigate(`/todo/${props.item.id}`);
    }
  }

  function deleteItem() {
    api
      .deleteTodo(props.item.id)
      .then((res) => {
        message.success("Delete successfully~");
        dispatch(onDeleteIcon(props.item.id));
      })
      .catch((err) => {});
  }

  function changeEditText(e) {
    setEditText(e.target.value);
  }

  const showModal = () => {
    setEditText(props.item.text);
    setIsModalOpen(true);
  };

  function pressEnter(e) {
    if (e.keyCode === PRESS_ENTER) {
      handleOk();
    }
  }

  const handleOk = () => {
    if (editText.trim() === "") {
      message.warning("Please input something~");
      return;
    }
    setSaveLoding(true);
    api
      .editTodo(props.item.id, editText)
      .then((res) => {
        message.success("Save successfully~");
        setIsModalOpen(false);
        setSaveLoding(false);
        dispatch(
          onSaveEdit({
            id: props.item.id,
            newText: editText,
          })
        );
      })
      .catch((err) => {
        setSaveLoding(false);
      });
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <div className={!props.item.done ? "todoItem" : "done todoItem"}>
        <div className={!props.item.done ? "text" : "textDone text"} onClick={changeStatus}>
          {props.item.text}
        </div>
        {!inDoneList && (
          <>
            <img src={require("./asset/edit.png")} alt="" onClick={showModal} />
          </>
        )}

        <Popconfirm placement="topLeft" title={Popconfirm_TITLE}  onConfirm={deleteItem} okText="Yes" cancelText="No">
          <img src={require("./asset/delete.png")} alt="" />
        </Popconfirm>
      </div>

      <Modal
        title="Edit Item"
        centered
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" loading={saveLoding} disabled={saveLoding} onClick={handleOk}>
            Save
          </Button>,
        ]}
      >
        <TextArea
          className="editTextArea"
          rows={4}
          placeholder="Add a ToDo.."
          maxLength={500}
          value={editText}
          onChange={changeEditText}
          onKeyUp={pressEnter}
        />
      </Modal>
    </>
  );
};

export default ToDoItem;
