import { Outlet, useNavigate } from "react-router-dom";
// import { NavLink } from "react-router-dom";
import "./Layout.css";
import React, { useEffect } from "react";
import { CheckCircleOutlined, InfoCircleOutlined, IssuesCloseOutlined, UnorderedListOutlined, AlignLeftOutlined } from "@ant-design/icons";
import { Menu, Drawer, Button } from "antd";
import { useState } from "react";
import { useLayoutEffect } from "react";

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const items = [
  getItem("Todo List", "/", <IssuesCloseOutlined />),
  {
    type: "divider",
  },
  getItem("Done List", "done", <CheckCircleOutlined />),
  {
    type: "divider",
  },
  getItem("About", "about", <InfoCircleOutlined />),
];

const Layout = () => {
  const navigate = useNavigate();
  const onClickMenu = (e) => {
    navigate(e.keyPath[0]);
    onClose()
  };

  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState("left");
  const showDrawer = () => {
    inPhoneScreen && setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  const onChange = (e) => {
    setPlacement(e.target.value);
  };

  // 响应
  const [inPhoneScreen, setInPhoneScreen] = useState(false);

  useEffect(() => {
    console.log();
    if (window.innerWidth < 600) {
      setInPhoneScreen(true);
    } else {
      setInPhoneScreen(false);
    }
  }, []);

  useLayoutEffect(() => {
    // 监听
    window.addEventListener("resize", () => {
      if (window.innerWidth < 600) {
        setInPhoneScreen(true);
      } else {
        setInPhoneScreen(false);
      }
    });
    // 销毁
    return () => window.removeEventListener("resize",()=> {});
  }, []);

  return (
    <div className="layout">
      <div className="header">
        <UnorderedListOutlined className="logo" onClick={showDrawer} /> ToDo
      </div>

      <div>
        <Drawer placement={placement} closable={false} onClose={onClose} open={open} key={placement}>
          <Menu
            className="navigatorMenu"
            onClick={onClickMenu}
            style={{
              width: 256,
              height: "100%",
            }}
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            mode="inline"
            items={items}
          />
        </Drawer>
        {!inPhoneScreen && (
          <Menu
            className="navigatorMenu"
            onClick={onClickMenu}
            style={{
              width: 256,
              height: "100%",
            }}
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            mode="inline"
            items={items}
          />
        )}
      </div>

      {/* <header>
        <NavLink className="link" to="/">
          TODOLIST
        </NavLink>
        <NavLink className="link" to="/done">
          DONELIST
        </NavLink>
        <NavLink className="link" to="/about">
          ABOUT
        </NavLink>
      </header> */}

      <Outlet></Outlet>
    </div>
  );
};

export default Layout;
