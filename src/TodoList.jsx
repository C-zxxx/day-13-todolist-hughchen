import ToDoItem from "./ToDoItem";
import InputBar from "./InputBar";
import "./TodoList.css";
import { useSelector } from "react-redux";
import * as React from "react";
import { useEffect } from "react";
import { AlignLeftOutlined } from "@ant-design/icons";
import { useTodo } from "./hooks/useTodo";

function TodoList() {
  const todoList = useSelector((state) => state.todo.todoList);
  const { reloadTodos } = useTodo();

  useEffect(() => {
    reloadTodos();
  }, []);

  return (
    <>
      <div className="todoList">
        <div className="title">Todo List</div>
        <InputBar className="inputBar" />

        <div className="todoItems">
          {todoList.map((item, index) => {
            return <ToDoItem item={item} key={index} index={index} />;
          })}
        </div>
      </div>
    </>
  );
}

export default TodoList;
