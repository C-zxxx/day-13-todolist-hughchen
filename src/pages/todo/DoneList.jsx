import ToDoItem from "../../ToDoItem";
import { useSelector } from "react-redux";
import * as React from "react";
import { useTodo } from "../../hooks/useTodo";
import { useEffect } from "react";
import "./DoneList.css";

const DoneList = () => {
  const todoList = useSelector((state) => state.todo.todoList);
  const { reloadTodos } = useTodo();

  const doneList = todoList.filter((item) => {
    return item.done;
  });

  useEffect(() => {
    reloadTodos();
  }, []);

  return (
    <>
      <div className="doneList">
        <div className="title">Done List</div>

        <div className="doneItems">
          {doneList.map((item, index) => {
            return <ToDoItem item={item} key={index} index={index} />;
          })}
        </div>
      </div>
    </>
  );
};

export default DoneList;
