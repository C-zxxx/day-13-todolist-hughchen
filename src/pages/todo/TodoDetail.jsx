import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Card, Tag } from "antd";

import "./TodoDetail.css";

const TodoDetail = () => {
  const navigate = useNavigate();
  const params = useParams();

  const id = params.id;
  const todoList = useSelector((state) => state.todo.todoList);

  const todoItem = todoList.find((element) => {
    return element.id.toString() === id;
  });

  useEffect(() => {
    if (!todoItem) {
      navigate("/404");
    }
  }, [todoItem, navigate]);

  return (
    <div className="todoDetail">
      <div className="title"> Detail </div>

      <Card
        className="detailCard"
        hoverable
        title="Todo Item"
        bordered={false}
        style={{
          "width": "90%",
        }}
      >
        <span className="itemTitle">Content:</span> <div className="text">{todoItem && todoItem.text}</div>
        <br></br>
        <span className="itemTitle">Status:</span>
        {todoItem && todoItem.done ? <Tag color="green">DONE</Tag> : <Tag color="orange">To be completed</Tag>}
      </Card>
    </div>
  );
};

export default TodoDetail;
