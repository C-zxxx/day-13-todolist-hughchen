import request from "./request";

export const loadTodos = () => {
  return request.get('')
}

export const toggleTodo = (id, done) => {
  return request.put(`${id}`, {
    done,
  })
}

export const addTodo = (item) => {
  return request.post('', item)
}

export const deleteTodo = (id) => {
  return request.delete(`${id}`)
}

export const editTodo = (id, text) => {
  return request.put(`${id}`, {
    text,
  })
}