import { message } from "antd";
import axios from "axios";

const request = axios.create({
  // mockapi.io
  // baseURL: 'https://64c1177cfa35860bae9ff242.mockapi.io/todo/', 
  baseURL: 'http://localhost:8081/todos', 
})

request.interceptors.response.use(
  (response) => response,
  (error) => {
    console.log("error.response.data",error.response.data);
    const msg = error.response.data
    if (msg) {
      message.error(msg) 
    }else{
      message.error("请求错误~")
    }
    return Promise.reject(error)
  }
)

export default request